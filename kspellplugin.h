#ifndef TESTQWEBSPELLCHECKER_H
#define TESTQWEBSPELLCHECKER_H


#include <QtGlobal>
#include <QtPlugin>
#include <sonnet/speller.h>
#include "qwebkitplatformplugin.h"




class KWebSpellChecker : public QWebSpellChecker {
  Q_OBJECT
public:
    Sonnet::Speller *m_speller;
    
    KWebSpellChecker();
    ~KWebSpellChecker();
    
    virtual bool isContinousSpellCheckingEnabled() const;
    virtual void toggleContinousSpellChecking();
    virtual void learnWord(const QString& word);
    virtual void ignoreWordInSpellDocument(const QString& word);
    virtual void checkSpellingOfString(const QString& word, int* misspellingLocation, int* misspellingLength);
	virtual QString autoCorrectSuggestionForMisspelledWord(const QString& word);
    virtual void guessesForWord(const QString& word, const QString& context, QStringList& guesses);

    virtual bool isGrammarCheckingEnabled();
    virtual void toggleGrammarChecking();
    virtual void checkGrammarOfString(const QString&, QList<GrammarDetail>&, int* badGrammarLocation, int* badGrammarLength);
};


class KWebKitPlatformPlugin : public QObject, public QWebKitPlatformPlugin {
  Q_OBJECT
  Q_INTERFACES(QWebKitPlatformPlugin)
  
public:
    KWebKitPlatformPlugin();
    ~KWebKitPlatformPlugin();
  
    bool supportsExtension(Extension) const;
    QObject* createExtension(Extension) const;
  
};

#endif