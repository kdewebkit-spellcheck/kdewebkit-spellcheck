#include <stdio.h>
#include <KDebug>
#include "kspellplugin.h"
#include <QTextBoundaryFinder>

#define methodDebug() kDebug("KWebSpellChecker: %s", __FUNCTION__)

/////////////////////////////
// KWebSpellChecker


KWebSpellChecker::KWebSpellChecker()
{
    m_speller = new Sonnet::Speller();
	kDebug() << "Client = " << m_speller->defaultClient() << endl;
	kDebug() << "Language = " << m_speller->defaultLanguage() << endl;
}

KWebSpellChecker::~KWebSpellChecker()
{
    delete m_speller;	
}

bool KWebSpellChecker::isContinousSpellCheckingEnabled() const
{
    return true;
}

void KWebSpellChecker::toggleContinousSpellChecking()
{
}

void KWebSpellChecker::learnWord(const QString& word)
{
	Q_UNUSED(word);
}

void KWebSpellChecker::ignoreWordInSpellDocument(const QString& word)
{
	Q_UNUSED(word);
}

static bool isValidWord(const QString &str)
{
    if(str.isEmpty() || (str.length() == 1 && !str[0].isLetter())) {
        return false;
    }
    const int length = str.length();
    for(int i = 0; i < length; ++i) {
        if(!str[i].isNumber()) {
            return true;
        }
    }
    // 'str' only contains numbers
    return false;
}

void KWebSpellChecker::checkSpellingOfString(const QString& word, int* misspellingLocation, int* misspellingLength)
{
	// sanity check
	if (misspellingLocation == NULL || misspellingLength == NULL)
		return;
	
    *misspellingLocation = -1;
    *misspellingLength = 0;
    
    kDebug() << word << endl;
    
    QTextBoundaryFinder finder =  QTextBoundaryFinder(QTextBoundaryFinder::Word, word);
    
    QTextBoundaryFinder::BoundaryReasons boundary = finder.boundaryReasons();
    int start = finder.position(), end = finder.position();
    bool inWord = (boundary & QTextBoundaryFinder::StartWord) != 0;
    while (finder.toNextBoundary() > 0) {
        boundary = finder.boundaryReasons();
        if ((boundary & QTextBoundaryFinder::EndWord) && inWord) {
            end = finder.position();
            QString str = finder.string().mid(start, end - start);
            if (isValidWord(str)) {
                #if 1
                qDebug() << "Word at " << start << " word = '"
                <<  str << "', len = " << str.length();
                #endif
                if (m_speller->isMisspelled(str))
                {
                    *misspellingLocation = start;
                    *misspellingLength = end - start;
                }
                return;
            }
            inWord = false;
        }
        if ((boundary & QTextBoundaryFinder::StartWord)) {
            start = finder.position();
            inWord = true;
        }
    }
}

QString KWebSpellChecker::autoCorrectSuggestionForMisspelledWord(const QString& word)
{
	QStringList words = m_speller->suggest(word);
	if (words.size() > 0)
		return words[0];
	else
		return QString("");
    
    
    return QString("");
}

void KWebSpellChecker::guessesForWord(const QString& word, const QString& context, QStringList& guesses)
{
	Q_UNUSED(context);

	QStringList words = m_speller->suggest(word);
	guesses = words;
}

bool KWebSpellChecker::isGrammarCheckingEnabled()
{
	return false;
}

void KWebSpellChecker::toggleGrammarChecking()
{
}

void KWebSpellChecker::checkGrammarOfString(const QString&, QList<GrammarDetail>&, int* badGrammarLocation, int* badGrammarLength)
{
	Q_UNUSED(badGrammarLocation);
	Q_UNUSED(badGrammarLength);
}


////////////////////////////////////////////
// KWebKitPlatformPlugin
KWebKitPlatformPlugin::KWebKitPlatformPlugin()
{
}

KWebKitPlatformPlugin::~KWebKitPlatformPlugin()
{
}


bool KWebKitPlatformPlugin::supportsExtension(Extension ext) const
{
    return ext == SpellChecker;
}

QObject* KWebKitPlatformPlugin::createExtension(Extension) const
{
    return new KWebSpellChecker();
}

Q_EXPORT_PLUGIN2(kwebspellchecker, KWebKitPlatformPlugin);


